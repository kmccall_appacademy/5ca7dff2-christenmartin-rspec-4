class Dictionary
	attr_accessor :entries

	def initialize
		@entries = {}
	end

	def add(entry)
		if entry.is_a?(String)
			@entries[entry]=nil
		else
			entry.each do |key,value|
			@entries[key]= value
			end
		end
	end


	def keywords
		@entries.keys.sort
	end

	def include?(entry)
    @entries.keys.include?(entry)
	end

	def find(x)
    answer = {}
    @entries.each do |key, value|
      if key == x
        answer[key] = value
      end
      if key[0..1] == x[0..1]
        answer[key] = value
      end
    end
    answer
	end


  def printable
   output = ""
   @entries.sort.each do |key, value|
     output += "[#{key}] \"#{value}\"\n"
   end
   output.chomp
  end

end
