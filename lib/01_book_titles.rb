class Book
  attr_accessor :title

  def title(title)
    @title = title
  end

  def title
    lower = %w(a an the to from on around into of and in)
    @title.split(" ").map.each_with_index do |word,i|
      if lower.include?(word) && i != 0
        word.downcase
      else
        word.capitalize
      end
    end.join(" ")
  end
end
